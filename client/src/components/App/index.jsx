import React, { Component } from 'react';
import memoizeOne from 'memoize-one';
import Board from 'components/Board';

export default class App extends Component {
  nextAgentMap = {1: 2, 2: 1};
  state = {
    isTraining: false,
    showBoard: false,
    trainFromScratch: false,
    episodesAmount: 1000,
    board: new Array(9).fill(0),
    currentAgentId: 1,
    history: []
  };

  getActionValues = memoizeOne((agentId, board) => {
    return this.agentsStateActionsValues[agentId][board.join(',')]
  });

  get actionValues() {
    return this.getActionValues(this.state.currentAgentId, this.state.board);
  }

  handleMove = cellId => this.setState(state => {
    const board = state.board.map((cell, i) => i === cellId ? state.currentAgentId: cell);
    const currentAgentId = this.nextAgentMap[state.currentAgentId];

    return {
      ...state,
      board,
      currentAgentId,
      history: [...state.history, {agentId: state.currentAgentId, board: state.board}]
    }
  });

  handleChange = (field, isCheckbox=false) => e => {
    const value = isCheckbox ? e.target.checked : e.target.value;
    this.setState({ [field]: value });
  };

  handleSubmit = async () => {
    try {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Cache-Control', 'no-cache, no-store');

      this.setState({ isTraining: true, showBoard: false });

      const { episodesAmount, trainFromScratch } = this.state;
      const query = `?episodes=${episodesAmount}&from_scratch=${trainFromScratch}`;
      const url = `http://localhost:8000/state_action_values${query}`;

      this.agentsStateActionsValues = await (await fetch(url, { method: 'GET', headers })).json();

      this.setState(state => ({
        ...state,
        showBoard: true,
        isTraining: false,
        board: [...state.board]
      }));
    } catch(e) {
      console.error(e);
      this.setState({ isTraining: false });
    }
  };

  goBack = (toStart=false) => () => this.setState(state => {
    const history = state.history;
    const { agentId, board } = state.history[toStart ? 0 : history.length - 1];

    return {
      ...state,
      board,
      currentAgentId: agentId,
      history: history.slice(0, toStart ? 1 : - 1)
    };
  });

  restart = this.goBack(true);

  undo = this.goBack();

  renderBoard() {
    const { showBoard, board } = this.state;

    return showBoard && <Board board={board} actionValues={this.actionValues} onClick={this.handleMove}/>;
  }

  render() {
    const { isTraining, episodesAmount, trainFromScratch, history }  = this.state;

    return <div>
      <div>
        <label>Episodes: </label>
        <input type="number" value={episodesAmount} onChange={this.handleChange('episodesAmount')} />
        <input type="checkbox" value={trainFromScratch} onChange={this.handleChange('trainFromScratch', true)} />
        <button onClick={this.handleSubmit}>Train</button>
        <button onClick={this.undo} disabled={history.length < 2}>Undo</button>
        <button onClick={this.restart}>Restart</button>
      </div>
      {isTraining ? 'Training...' : this.renderBoard()}
    </div>
  }
}