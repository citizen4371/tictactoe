import React from 'react';
import classNames from 'classnames';
import styles from './styles.css';

const captionsMap = {
  1: 'X',
  2: '0'
};

export default function Board({ board, actionValues, onClick }) {
  return <div className={styles.board}>
    {board.map((cell, i) => <div disabled={!!cell} key={i} onClick={() => onClick(i)}>
      {cell ? captionsMap[cell] : (actionValues[i] === undefined ? '?' : actionValues[i])}
    </div>)}
  </div>;
}