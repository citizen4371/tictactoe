const path = require('path');
const babelEnvPreset = ['env', {
  targets: {
    browsers: ["last 2 versions"]
  }
}];

const babelLoader = {
  loader: 'babel-loader',
  options: {
    presets: [babelEnvPreset, 'react'],
    plugins: ['transform-object-rest-spread', 'transform-class-properties']
  }
};

const webpack = require('webpack');

module.exports = {
  entry: ['babel-polyfill', path.resolve(__dirname, 'src/index.jsx')],
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, '/dist')
  },
  devtool: 'source-map',
  devServer: {
    publicPath: '/dist/'
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      utils: path.resolve(__dirname, 'src/utils'),
      components: path.resolve(__dirname, 'src/components')
    }
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [/node_modules/],
        use: [babelLoader]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: '[local]_[hash:base64:5]', // Add naming scheme
            },
          }
        ],
      }
    ]
  }
};
