from collections.abc import Mapping


class IdenticalMapping(Mapping):
    def __init__(self, create_identical_values):
        self._values = []
        self._cache = {}
        self._create_identical_values = create_identical_values

    def __getitem__(self, key):
        try:
            return self._values[self._cache[key]]
        except LookupError:
            identical_values = (key, *self._create_identical_values(key))
            self._values.append(identical_values)

            for v in self._values[-1]:
                self._cache[v] = len(self._values) - 1

            return identical_values

    def __len__(self):
        return len(self._values)

    def __iter__(self):
        yield from iter(self._values)
