import abc
import random
import math


class Policy:
    @abc.abstractmethod
    def get_action(self, state, actions, state_action_values):
        pass


class RandomPolicy(Policy):
    def get_action(self, _, actions, s_a_v):
        return random.choice(actions)


class EpsilonGreedyPolicy(Policy):
    def __init__(self, get_epsilon=lambda k: 100 / (100 + k)):
        self._get_epsilon = get_epsilon

    @staticmethod
    def _get_best_actions(s, actions, s_a_v):
        best_actions = []
        cur_max = -math.inf

        for a in actions:
            value = s_a_v.get((s, a), 0)
            if value > cur_max:
                best_actions = [a]
                cur_max = value
            elif value == cur_max:
                best_actions.append(a)

        return best_actions

    def get_action(self, s, actions, s_a_v, episode_number):
        eps = self._get_epsilon(episode_number)
        return random.choice(actions if random.random() < eps else self._get_best_actions(s, actions, s_a_v))
