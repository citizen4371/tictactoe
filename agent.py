import abc
from functools import lru_cache
from mean_accumulator import MeanAccumulator
from utils import groupby
from identical_mapping import IdenticalMapping


class Agent(object):
    def __init__(self, env, decay=0.9, create_identical_state_actions=None):
        self._on_finish_callback = None
        self._env = env
        self._token = self._env.register(self.on_env_change)
        self._state_action_reward_history = []
        self._decay = decay
        self._last_state = None
        self._last_action = None
        self._episode_number = 1
        self._identical_mapping = create_identical_state_actions and IdenticalMapping(create_identical_state_actions)

    @abc.abstractmethod
    def _get_action(self):
        pass

    def _get_identical_state_actions(self, s_a):
        return [s_a] if self._identical_mapping is None else self._identical_mapping[s_a]

    def _update(self, state, action, reward, new_state):
        self._state_action_reward_history.append((state, action, reward, new_state))

    def _finish_episode(self):
        self._episode_number += 1
        self._on_finish_callback and self._on_finish_callback()

    def on_finish(self, callback):
        self._on_finish_callback = callback

    def on_env_change(self, new_state, available_actions, reward):
        if self._last_state and self._last_action is not None:
            self._update(self._last_state, self._last_action, reward, new_state, available_actions)

        if not len(available_actions):
            self._finish_episode()
            return

        self.make_move(new_state, available_actions)

    def make_move(self, state=None, actions=None):
        self._last_state = state or self._env.state
        self._last_action = self._get_action(
            self._last_state,
            actions or self._env.get_available_actions()
        )

        self._env.dispatch(self._token, self._last_action, self.on_env_change)

    def reset(self):
        self._state_action_reward_history = []
        self._last_state = None
        self._last_action = None
        self._episode_number = 1


class MonteCarloAgent(Agent):
    def __init__(self, env, policy, decay=0.9, create_identical_state_actions=None):
        super().__init__(env, decay, create_identical_state_actions)
        self._policy = policy
        self._state_action_values = MeanAccumulator()

    def _get_action(self, state, available_actions):
        return self._policy.get_action(state, available_actions, self._state_action_values, self._episode_number)

    def _finish_episode(self):
        self._update_state_action_values()
        super()._finish_episode()

    def _update_state_action_values(self):
        aggregated_return = 0

        for i, (state, action, reward, _) in enumerate(reversed(self._state_action_reward_history)):
            aggregated_return = reward + self._decay * aggregated_return
            for s_a in self._get_identical_state_actions((state, action)):
                self._state_action_values[s_a] = aggregated_return

        self._state_action_reward_history = []

    @property
    def state_action_values(self):
        grouped = groupby(self._state_action_values.items(), key=lambda x: x[0][0])
        return {','.join(map(str, s)): {a: v for (_, a), v in g} for s, g in grouped.items()}

    def reset(self):
        super().reset()
        self._state_action_values = MeanAccumulator()


class SarsaAgent(Agent):
    def __init__(self, env, policy, decay=0.9, learning_rate=0.1, create_identical_state_actions=None):
        super().__init__(env, decay, create_identical_state_actions)
        self._policy = policy
        self._q = {}
        self._learning_rate = learning_rate

    @lru_cache(maxsize=1)
    def _get_action_cached(self, state, available_actions, step):
        return self._policy.get_action(state, available_actions, self._q, self._episode_number)

    def _get_action(self, state, available_actions):
        return self._get_action_cached(state, available_actions, len(self._state_action_reward_history))

    def _update(self, state, action, reward, new_state, available_actions):
        super()._update(state, action, reward, new_state)

        curr_value = self._q.get((state, action), 0)                      # Q(S, A)
        if len(available_actions):
            next_action = self._get_action(new_state, available_actions)  # A'
            next_value = self._q.get((new_state, next_action), 0)         # Q(S', A')
        else:
            next_value = 0

        for s_a in self._get_identical_state_actions((state, action)):
            # Q(S, A) = Q(S, A) + α(R + γQ(S', A') - Q(S, A))
            self._q[s_a] = curr_value + self._learning_rate * (reward + self._decay * next_value - curr_value)

    @property
    def state_action_values(self):
        grouped = groupby(self._q.items(), key=lambda x: x[0][0])
        return {','.join(map(str, s)): {a: v for (_, a), v in g} for s, g in grouped.items()}
