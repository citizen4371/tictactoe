import asyncio
from pprint import pprint
from main import Main

loop = asyncio.get_event_loop()
main = Main(loop)

main.train(10)

try:
    loop.run_forever()
finally:
    loop.run_until_complete(loop.shutdown_asyncgens())

pprint(main.training_data)

