from environment import TicTacToeEnvironment
from agent import MonteCarloAgent, SarsaAgent
from policies import EpsilonGreedyPolicy, RandomPolicy
from utils import create_identical_state_actions


class Main(object):
    def __init__(self, loop):
        self._loop = loop

        self._env = TicTacToeEnvironment(loop)
        self._env.subscribe(self._on_env_update)

        self.agent1 = SarsaAgent(self._env, EpsilonGreedyPolicy(lambda _: 0.1), create_identical_state_actions=create_identical_state_actions)
        self.agent1.on_finish(self._train_episode)
        self.agent2 = SarsaAgent(self._env, EpsilonGreedyPolicy(lambda _: 0.1), create_identical_state_actions=create_identical_state_actions)

        self._episodes_amount = 0
        self._episodes_trained = 0
        self.history = []

    def _on_env_update(self, state):
        try:
            self.history[-1].append(state)
        except Exception:
            pass

    def _train_episode(self):
        if self._episodes_trained < self._episodes_amount:
            self._episodes_trained += 1
            self._env.reset()
            self.history.append([])
            self.agent1.make_move()
        else:
            self._loop.stop()

    def train(self, episodes_amount=10, from_scratch=False):
        if from_scratch:
            self.agent1.reset()
            self.agent2.reset()
            self._episodes_trained = 0
            self._episodes_amount = episodes_amount
        else:
            self._episodes_amount += episodes_amount

        self._train_episode()

    @property
    def training_data(self):
        return {
            "1": self.agent1.state_action_values,
            "2": self.agent2.state_action_values
        }
