from collections.abc import MutableMapping
from collections import defaultdict, Counter


class MeanAccumulator(MutableMapping):
    def __init__(self):
        self._sums = defaultdict(int)
        self._counts = Counter()

    def __setitem__(self, key, value):
        self._counts[key] += 1
        self._sums[key] += value

    def __getitem__(self, key):
        s = self._sums.get(key, None)
        count = self._counts.get(key, None)

        return s and count and s / count

    def __delitem__(self, key):
        del self._sums[key]
        del self._counts[key]

    def __len__(self):
        return len(self._sums)

    def __iter__(self):
        yield from iter(self._sums)
