from collections import defaultdict
from functools import reduce
import numpy as np


def _id(val):
    return val


def groupby(iterable, key=_id, mapper=_id):
    def reducer(acc, item):
        acc[key(item)].append(mapper(item))
        return acc

    return reduce(reducer, iterable, defaultdict(list))


def create_identical_state_actions(state_action):
    def recreate_state_action(state_np):
        flat = state_np.flatten()
        action = int(np.where(flat == -1)[0][0])
        flat[action] = 0
        return tuple(flat), action

    state, action = state_action
    state_np = np.array(state)
    state_np[action] = -1
    state_np = state_np.reshape((3, 3))

    for rotation_amount in range(1, 4):
        yield recreate_state_action(np.rot90(state_np, rotation_amount))

    for axis in range(0, 2):
        yield recreate_state_action(np.flip(state_np, axis))
        yield recreate_state_action(np.flip(np.rot90(state_np, 1), axis))