import sys
import asyncio
from quick_server import create_server, msg, setup_restart
from main import Main

setup_restart()
# sets up restart functionality (if not called the `restart` command of the server needs external help to work)
# should be the first real executed command in the script
# some services, like heroku, don't play well with this command and it should not be called if in such an environment

addr = ''
port = 8000
server = create_server((addr, port), parallel=True)
server.bind_path('/', 'client')
server.add_default_white_list()
server.link_empty_favicon_fallback()
server.cross_origin = True
server.suppress_noise = True
server.report_slow_requests = True

loop = asyncio.get_event_loop()
main = Main(loop)


@server.json_get('/state_action_values')
def state_action_values(req, args):
    try:
        episodes_amount = int(args['query']['episodes'])
        from_scratch = args['query']['from_scratch'] == 'true'
    except:
        pass

    main.train(episodes_amount, from_scratch)

    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())

    return main.training_data


msg("{0}", " ".join(sys.argv))
msg("starting server at {0}:{1}", addr if addr else 'localhost', port)
try:
    server.serve_forever()
finally:
    msg("shutting down..")
    msg("{0}", " ".join(sys.argv))
    server.server_close()
