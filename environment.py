class TicTacToeEnvironment(object):
    def __init__(self, loop):
        self._players = {}
        self._subscriptions = []
        self._current_token = 1
        self._board = [0] * 9
        self._game_over = False
        self._winner_token = None
        self.loop = loop

    @property
    def state(self):
        return tuple(self._board)

    def _generate_token(self):
        token = self._current_token
        self._current_token += 1
        return token

    def _get_winner_token(self):
        s = self._board

        for i in [0, 3, 6]:
            if s[i] and s[i] == s[i + 1] and s[i] == s[i + 2]:
                return s[i]

        for i in [0, 1, 2]:
            if s[i] and s[i] == s[i + 3] and s[i] == s[i + 6]:
                return s[i]

        if s[0] and s[0] == s[4] and s[0] == s[8]:
            return s[0]

        if s[2] and s[2] == s[4] and s[2] == s[6]:
            return s[2]

        return None

    def get_available_actions(self):
        if self._game_over:
            return tuple()

        return tuple([cell_idx for cell_idx, cell_value in enumerate(self._board) if cell_value == 0])

    def _get_reward(self, token):
        if not self._game_over:
            return 0

        if self._winner_token is None:
            return 0

        return 1 if self._winner_token == token else -1

    def dispatch(self, token, action, callback):
        cell_idx = action

        if self._game_over or self._board[cell_idx] != 0:
            return

        self._players[token] = callback
        self._board[cell_idx] = token

        free_cells_amount = len([cell for cell in self._board if cell == 0])
        self._winner_token = None if free_cells_amount > 4 else self._get_winner_token()
        self._game_over = self._winner_token is not None or free_cells_amount == 0

        state = self.state

        for t, callback in self._players.items():
            if self._game_over or t != token:
                self.loop.call_soon(callback, state, self.get_available_actions(), self._get_reward(t))

        for callback in self._subscriptions:
            callback(state)

    def register(self, callback):
        if len(self._players) < 2:
            token = self._generate_token()

            self._players[token] = callback

            return token

    def subscribe(self, callback):
        self._subscriptions.append(callback)

    def reset(self):
        self._board = [0] * 9
        self._game_over = False
        self._winner_token = None
